﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignment1a_n01306505.aspx.cs" Inherits="assignment1a_n01306505.assignment1a_n01306505" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Hotel Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
            <p>Select your dates</p>
            <h4>"Check-in Date:"</h4>xxxxxxxxxxxxxxxxxx
            <asp:TextBox runat="server" ID="ArrivalDate" placeholder="MM/DD/YYYY"></asp:TextBox>
        
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter arrival date" ControlToValidate="arrivalDate" ID="validatorArrivalDate"></asp:RequiredFieldValidator>
            <br />
            <h4>"Check-out Date:"</h4>
            <asp:TextBox runat="server" ID="DepartureDate" placeholder="MM/DD/YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter departure date" ControlToValidate="DepartureDate" ID="validatordepartureDate"></asp:RequiredFieldValidator>
            <br />
            <asp:DropDownList runat="server" ID="DropDownRooms">
                <asp:ListItem Value="Room1" Text="1 Room"></asp:ListItem>
                <asp:ListItem Value="Room2" Text="2 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room3" Text="3 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room4" Text="4 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room5" Text="5 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room6" Text="6 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room7" Text="7 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room8" Text="8 Rooms"></asp:ListItem>
                <asp:ListItem Value="Room9" Text="9+ Rooms"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList runat="server" ID="DropDownGuest1">
                <asp:ListItem Value="AGuest1" Text="1-Adult"></asp:ListItem>
                <asp:ListItem Value="AGuest2" Text="2-Adults"></asp:ListItem>
                <asp:ListItem Value="AGuest3" Text="3-Adults"></asp:ListItem>
                <asp:ListItem Value="Aguest4" Text="4-Adults"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList runat="server" ID="DropDownGuest2">
                <asp:ListItem Value="Child0" Text="0-Children"></asp:ListItem>
                <asp:ListItem Value="Child1" Text="1-Child"></asp:ListItem>
                <asp:ListItem Value="Child2" Text="2-Children"></asp:ListItem>
                <asp:ListItem Value="Child3" Text="3-Children"></asp:ListItem>
                <asp:ListItem Value="Child4" Text="4-Children"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:TextBox runat="server" ID="clientLName" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your Last Name" ControlToValidate="clientLName" ID="validatorCLName"></asp:RequiredFieldValidator>
            <br />
            <asp:TextBox runat="server" ID="clientFName" placeholder="First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="clientFName" ID="validatorCFName"></asp:RequiredFieldValidator>
            <br />
                                        
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Phone"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorCPhone"></asp:RequiredFieldValidator>
            <br />
            <asp:CompareValidator runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="9051231234" ErrorMessage="This is an invalid phone number"></asp:CompareValidator>
            <br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter an Email" ControlToValidate="clientEmail" ID="validatorCEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <asp:DropDownList runat="server" ID="DropDownCountry">
                <asp:ListItem Value="Australia" Text="Australia"></asp:ListItem>
                <asp:ListItem Value="Canada" Text="Canada"></asp:ListItem>
                <asp:ListItem Value="Denmark" Text="Denmark"></asp:ListItem>
                <asp:ListItem Value="Malaysia" Text="Malaysia"></asp:ListItem>
                <asp:ListItem Value="USA" Text="USA"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:TextBox runat="server" ID="clientProv" placeholder="State/Province"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientProv" ErrorMessage="Please enter State or Province"></asp:RequiredFieldValidator>
            <br />   

                                  
             <asp:TextBox runat="server" ID="clientAddress" placeholder="Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientAddress" ErrorMessage="Please enter an Address"></asp:RequiredFieldValidator>
            <br />   
            <asp:TextBox runat="server" ID="clientCity" placeholder="City"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientCity" ErrorMessage="Please enter City"></asp:RequiredFieldValidator>
            <br />  
            <asp:TextBox runat="server" ID="clientZip" placeholder="ZipCode"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientZip" ErrorMessage="Please enter City"></asp:RequiredFieldValidator>
            <br />   
                                 




            <div id="Div1" runat="server">
            <asp:CheckBox runat="server" ID="CheckBox1" Text="Text me my reservation" />
            <asp:CheckBox runat="server" ID="CheckBox2" Text="Get exclusive deals via email" />
            <asp:CheckBox runat="server" ID="CheckBox3" Text="Sign up for Free sightseeing Tour" />
            </div>

            <div>
            <h4>"Payment method:"</h4>
            <asp:RadioButton runat="server" Text="Visa" GroupName="via"/>
            <asp:RadioButton runat="server" Text="MasterCard" GroupName="via"/>
            </div>
            <br /> 
            

            
        






            
            
        

    </form>
</body>
</html>